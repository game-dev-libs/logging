package logging;

import com.felixcool98.logging.Logs;
import com.felixcool98.logging.logger.ConsoleLogger;
import com.felixcool98.logging.logtype.LogType;

public class Test {
	public static void main(String[] args) {
		new ConsoleLogger().log(new LogType(), "Hello world");
		Logs.log("h");
	}
}
