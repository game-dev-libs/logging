package com.felixcool98.logging.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.felixcool98.logging.logger.Logger.AbstractLogger;
import com.felixcool98.logging.logtype.LogType;

public class FileLogger extends AbstractLogger {
	private File file;
	
	
	public FileLogger(File file) {
		this.file = file;
	}
	
	
	@Override
	protected void logThis(StackTraceElement trace, LogType type, String message) {
		if(!type.tags.isEmpty()) {
			String tagString = "";
			
			for(String tag : type.tags) {
				tagString += "["+tag+"] ";
			}
			
			message = tagString+message;
		}
		
		FileWriter writer = null;
		
		String link = trace.getClassName()+" ("+trace.getFileName()+":"+trace.getLineNumber()+")";
		
		try {
			writer = new FileWriter(file, true);
			writer.write(link+message);
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
			if(writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
