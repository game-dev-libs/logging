package com.felixcool98.logging.logger;

import com.felixcool98.logging.logger.Logger.AbstractLogger;
import com.felixcool98.logging.logtype.LogType;

public class ConsoleLogger extends AbstractLogger {
	@Override
	protected void logThis(StackTraceElement trace, LogType type, String message) {
		String link = "("+trace.getFileName()+":"+trace.getLineNumber()+")";
		
		if(!type.tags.isEmpty()) {
			String tagString = "";
			
			for(String tag : type.tags) {
				tagString += "["+tag+"] ";
			}
			
			message = tagString+message;
		}
		
		message = link+" "+message;
		
		if(type.equals(LogType.EXCEPTION)) 
			System.err.println(message);
		else 
			System.out.println(message);
	}
}