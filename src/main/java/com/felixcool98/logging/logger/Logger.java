package com.felixcool98.logging.logger;

import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.utility.filter.Filter;

public interface Logger {
	public default void log(LogType type, String message) {
		log(1, type, message);
	}
	
	public default void log(int traceStackHeight, LogType type, String message) {
		log(new Throwable().getStackTrace()[1+traceStackHeight], type, message);
	}
	
	public void log(StackTraceElement trace, LogType type, String message);
	
	public void setFilter(Filter<LogType> filter);
	
	
	
	public abstract class AbstractLogger implements Logger{
		private Filter<LogType> filter;
		
		
		@Override
		public void setFilter(Filter<LogType> filter) {
			this.filter = filter;
		}
		
		
		protected abstract void logThis(StackTraceElement trace, LogType type, String message);
		
		
		@Override
		public void log(StackTraceElement trace, LogType type, String message) {
			if(filter != null && !filter.valid(type))
				return;
			
			logThis(trace, type, message);
		}
	}
}
