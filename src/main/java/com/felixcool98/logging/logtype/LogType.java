package com.felixcool98.logging.logtype;

import java.util.HashSet;

public class LogType {
	public static final LogType EXCEPTION = new LogType("Exception");
	public static final LogType WARNING = new LogType("Warning");
	public static final LogType MESSAGE = new LogType("Message");
	
	
	public final HashSet<String> tags = new HashSet<>();
	
	
	public LogType(String...tags) {
		addTags(tags);
	}
	
	
	public void addTags(String... tags) {
		if(tags == null)
			return;
		
		for(String tag : tags) {
			this.tags.add(tag);
		}
	}
	
	
	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}
}
