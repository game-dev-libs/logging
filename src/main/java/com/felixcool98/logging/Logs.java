package com.felixcool98.logging;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.logging.logger.ConsoleLogger;
import com.felixcool98.logging.logger.Logger;
import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.utility.filter.Filter;

public class Logs {
	private static List<Logger> loggers = new LinkedList<>();
	
	
	static {
		loggers.add(new ConsoleLogger());
	}
	
	
	public static void add(Logger logger) {
		loggers.add(logger);
	}
	public static void clear() {
		loggers.clear();
	}
	public static void remove(Logger logger) {
		loggers.remove(logger);
	}
	
	
	public static List<Logger> getAll(){
		return loggers;
	}
	
	
	public static void log(LogType type, String message) {
		log(1, type, message);
	}
	private static void log(int traceStackHeight, LogType type, String message) {
		for(Logger logger : loggers) {
			logger.log(traceStackHeight+1, type, message);
		}
	}
	public static void log(String[] tags, int level, String message) {
		log(1, new LogType(tags), message);
	}
	public static void log(Exception e) {
		log(1, LogType.EXCEPTION, e.getMessage());
	}
	public static void log(String text) {
		log(1, LogType.MESSAGE, text);
	}
	
	
	public static void setFilter(Filter<LogType> filter) {
		for(Logger logger : loggers) {
			logger.setFilter(filter);
		}
	}
}
